<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>The Malien Tweets Filter</title>

    <!-- On importe bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/styles.css"/>

    <!-- On définit le stryle CSS de la page -->
    <style>
      .tweetImg {
        max-width: 300px;
        max-height: 150px;
      }

      .imgProfil {
        vertical-align: center;
        margin-top: 20px;
      }

      select {
        margin-right: 10px;
      }

      .oui {
        margin-top: 20px;
      }

      .cbTweets {
        margin-right: 10px;
      }
    </style>

  </head>

  <body>
    <!-- Définition du menu -->
    <nav class="navbar navbar-light bg-light justify-content-between">
      <!-- Image en haut à droite -->
      <a class="navbar-brand" href="#">
        <img src="img/malo.png" alt="The big bird" width="60px" height="60px"/>
      </a>
      <!-- Formulaire de recherche -->
      <form class="form-inline" action="index.php" method="post">
        <!-- Si la case "Filter on user" a été cochée à la recherche précédente on la coche de base -->
        <?php
        if ($_POST['isUser'] == "true")
        {
        ?>
          <!-- Case "Filter on user" pour filter sur les tweets d'une seule personne-->
          <div class="form-check cbTweets">
            <input type="checkbox" name="isUser" class="form-check-input" id="isUserCB" value="true" checked>
            <label class="form-check-label" for="isUserCB">Filter on user</label>
          </div>
        <?php
        }
        else
        {
        ?>
          <!-- Case "Filter on user" pour filter sur les tweets d'une seule personne-->
          <div class="form-check cbTweets">
            <input type="checkbox" name="isUser" class="form-check-input" id="isUserCB" value="true">
            <label class="form-check-label" for="isUserCB">Filter on user</label>
          </div>
        <?php
        }
        ?>
        <!-- Liste déroulante pour choisir le nombre de tweets affichés -->
        <select class="custom-select" id="nbTweets" name="nbTweets">
          <option value="20" selected>20</option>
          <option value="30" >30</option>
          <option value="40" >40</option>
          <option value="50" >50</option>
          <option value="60" >60</option>
          <option value="70" >70</option>
          <option value="80" >80</option>
          <option value="90" >90</option>
          <option value="100" >100</option>
        </select>
        <!-- Liste déroulante pour choisir la langue des tweets retournés parmis Français, Anglais et Japonais -->
        <select class="custom-select" id="lang" name="lang">
          <?php
          // On définit la valeur de base de la langue à celle qui avait été choisie précédemment (Ici Japonais)
          if ($_POST['lang'] == "ja")
          {
          ?>
            <option value="fr">Français</option>
            <option value="en">English</option>
            <option value="ja" selected>日本の</option>
          <?php
          }
          // On définit la valeur de base de la langue à celle qui avait été choisie précédemment (Ici Anglais)
          else if ($_POST['lang'] == "en")
          {
          ?>
            <option value="fr">Français</option>
            <option value="en" selected>English</option>
            <option value="ja">日本の</option>
          <?php
          }
          // On définit la valeur de base de la langue à celle qui avait été choisie précédemment (Ici Français (Valeur de base))
          else
          {
          ?>
            <option value="fr" selected>Français</option>
            <option value="en">English</option>
            <option value="ja">日本の</option>
          <?php
          }
          ?>
        </select>
        <!-- Liste déroulante pour choisir le type de résultat que l'on souhaite parmis tweets populaires, tweets récents ou un mix des deux -->
        <select class="custom-select" id="resultType" name="resultType">
        <?php
          // On définit la valeur de base du type de résultat à celle qui avait été choisie précédemment (Ici tweets populaires)
          if ($_POST['resultType'] == "popular")
          {
          ?>
            <option value="mixed">Recent and popular Tweets</option>
            <option value="recent">Only recent Tweets</option>
            <option value="popular" selected>Only popular Tweets</option>
          <?php
          }
          // On définit la valeur de base du type de résultat à celle qui avait été choisie précédemment (Ici tweets récents)
          else if ($_POST['resultType'] == "recent")
          {
          ?>
            <option value="mixed">Recent and popular Tweets</option>
            <option value="recent" selected>Only recent Tweets</option>
            <option value="popular">Only popular Tweets</option>
          <?php
          }
          // On définit la valeur de base du type de résultat à celle qui avait été choisie précédemment (Ici mix des deux (valeur de base))
          else
          {
          ?>
            <option value="mixed" selected>Recent and popular Tweets</option>
            <option value="recent">Only recent Tweets</option>
            <option value="popular">Only popular Tweets</option>
          <?php
          }
          ?>
        </select>
        <!-- Champ pour entrer ce que l'on veut chercher -->
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Enter keywords" name="search" value=<?php echo $_POST['search'] ?>>
        <!-- Bouton de lancer la recherche -->
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>
    </nav>
    <main>

<?php
    require_once('TwitterAPIExchange.php');

    // Définition des tokens de connexion à l'API tweeter
    $settings = array(
        'oauth_access_token' => "1105495164703920129-6nFTPKrRoxJZhrfbIyUSdppB2ZJK3V",
        'oauth_access_token_secret' => "EUHHanxtDvTANPPqi79ro0MfSQKbtsXVwrMmVqzRDYNgD",
        'consumer_key' => "9c8N6ezUjvTGh9hqum9YQkNjI",
        'consumer_secret' => "EHDzAhOGT2py7vqwd5mP3NmLche2LQUHQg6Zoxho03qhVBxAAS"
    );

    //$words = "";

    // Url de l'API tweeter
    $url = "https://api.twitter.com/1.1/search/tweets.json";

    // Type de méthode pour l'appel à l'API (Ici GET)
    $requestMethod = "GET";

    // S'il n'y a rien de rentré dans le champ de recherche on fait une recherche automatique sur le mot "Database"
    if ($_POST['search'] == null)
    {
      $getfield = '?q=Database&lang='.$_POST['lang'].'&result_type='.$_POST['resultType'].'&tweet_mode=extended&count='.$_POST['nbTweets'];
    }
    // Si une recherche est rentrée on fait la recherche dessus
    else
    {
      // Si la checkedbox "Filter on user" n'est pas coché on fait une recherche sur les tweets contenant le mot recherché
      if ($_POST['isUser'] == null)
      {
        $getfield = '?q='.$_POST['search'].'&lang='.$_POST['lang'].'&result_type='.$_POST['resultType'].'&tweet_mode=extended&count='.$_POST['nbTweets'];
      }
      // Si la checkbox "Filter on user" est coché on fait la recherche sur les tweets de la personne recherchée
      else
      {
        $getfield = '?q=from:'.$_POST['search'].'&result_type='.$_POST['resultType'].'&tweet_mode=extended&count='.$_POST['nbTweets'];
      }
    }
    
    // Appel au package aidant à la connexion à l'API twitter
    $twitter = new TwitterAPIExchange($settings);
    $string = json_decode($twitter->setGetfield($getfield)
    ->buildOauth($url, $requestMethod)
    ->performRequest(),$assoc = TRUE);
    // S'il y a un problème on retourne une erreur
    if(array_key_exists("errors", $string)) 
    {
        echo "<h3>Sorry, there was a problem.</h3><p>Twitter returned the following error message:</p><p><em>".$string["errors"][0]["message"]."</em></p>";
        exit();
    }

    // Variable contenant la partie qui nous interresse parmis la réponse de Twitter
    $statuses = $string["statuses"];

    // Boucle de parcours de tous les tweets qu'on a reçu en réponse
    foreach ($statuses as $tweet)
    {
      // Définition de l'adresse de l'image de profil de la personne ayant tweeté/retweeté
        $img = '<img src='.$tweet["user"]["profile_image_url"].' alt="Image de profil" class="rounded-circle" width="90px" height="90px">';
        ?>
        <div class="card mb-3 container oui" width="500px" height="200px">
        <div class="row">
          <!-- Affichage de l'image de profil -->
          <div class="imgProfil col-md-4">
           <?php
            echo $img;
           ?>
          </div>
          <div class="col-md-8">
            <div class="card-body">
              <!-- Affichage du nom -->
              <h6 class="card-title"><?php echo $tweet["user"]["name"]." @";echo $tweet["user"]["screen_name"] ?></h6>
              <!-- Affichage du tweet en lui même -->
              <p class="card-text"><?php echo $tweet["full_text"] ?></p>
              <!-- Test pour savoir s'il y a une ou plusieurs images avec le tweet -->
              <?php if ($tweet["entities"]["media"] != [])
              {
                $images = $tweet["entities"]["media"];
                foreach ($images as $image)
                {
                  $img2 = '<img src='.$image["media_url"].' alt="Image du tweet" class="tweetImg" />';
                ?>
                <div class="row">
                  <!-- Affichage des différentes images du tweet -->
                  <?php echo $img2; ?>
                </div>
                <?php
                }
              }
              ?>
              <!-- Affichage de la date du tweet formatée pour correspondre à une date facile à lire -->
              <p class="card-text"><small class="text-muted"><?php echo strftime("%d %B %Y %Hh%M", strtotime($tweet["created_at"])+60*60) ?></small></p>
            </div>
          </div>
        </div>
      </div>
      <?php
      // Récupération de tous les mots de tous les tweets pour afficher les 3 mots les plus utilisés (Pas fini)
      //$words = $words." ".$tweet["full_text"];
    }
      // Affichage du nombre d'apparition de chaques mots pour afficher les 3 mots les plus utilisés (Pas fini)
      //var_dump(array_count_values(str_word_count($words, 1)));
        ?>
    </main>
  </body>
</html>

<!-- Tentative d'afficher les tweets en direct sans avoir besoin de faire une nouvelle recherche -->

<!--	use Nticaric\Twitter\TwitterStream;
//
//	$stream = new TwitterStream(array(
//	    'consumer_key'    => '9c8N6ezUjvTGh9hqum9YQkNjI',
//	    'consumer_secret' => 'EHDzAhOGT2py7vqwd5mP3NmLche2LQUHQg6Zoxho03qhVBxAAS',
//	    'token'           => '1105495164703920129-6nFTPKrRoxJZhrfbIyUSdppB2ZJK3V',
//	    'token_secret'    => 'EUHHanxtDvTANPPqi79ro0MfSQKbtsXVwrMmVqzRDYNgD'
//	));
//
//	$res = $stream->getStatuses(['track' => 'car'], function($tweet) {
//		//prints to the screen statuses as they come along
//		print_r($tweet);
//	}); -->

